
let calculator = {};


const Sumar = (num1, num2) => {
    let resultado;
    resultado = num1 + num2;
    console.log(`El resultado es ${resultado}`);
    return resultado;
}

const Restar = (num1, num2) => {
    let resultado;
    resultado = num1 - num2;
    console.log(`El resultado es ${resultado}`);
    return resultado;
}


const Multiplicar = (num1, num2) => {
    let resultado;
    resultado = num1 * num2;
    console.log(`El resultado es ${resultado}`);
    return resultado;
}


const Dividir = (num1, num2) => {
    let resultado;
    resultado = num1 / num2;

    if (num2 === 0) {
        console.log("No podes dividir por cero");
        return resultado = "No podes dividir por cero";
    } else {
        console.log(`El resultado es ${resultado}`);
    }
    return resultado;
}



calculator.sumar = Sumar;
calculator.restar = Restar;
calculator.multiplicar = Multiplicar;
calculator.dividir = Dividir;

module.exports = calculator;